# Java Interview
# <span id="java_category">目录</span>
[1. GC机制](#java_gc_1)

[2. JVM 内存区域的划分，哪些区域会发生 OOM](#java_jvm_2)

[3. 类加载过程](#java_3_classloader)

[4. 双亲委派模型](#java_4_doubleParent)

[5. Java 中的集合类](#java_5_set)

[6. Java 死锁的产生，如何定位、修复，死锁 demo](#java_6_synchronized)

[7. sleep & wait 的区别](#java_7_sleep&wait)

[8. join 的用法](#java_8_join)

[9. volatile和synchronize的区别](#java_9_volatile&synchronized)

[10. Java中的线程池](#java_10_threadPool)

[11. 线程通信](#java_11_threadCommunication)

[12. Java中的并发集合](#java_12_concurrentSet)

[13. Java中生产者与消费者模式](#java_13_producer&consumer)

[14. final、finally、finalize区别](#java_14_finalXXX)

[15. Java 单例模式](#java_15_singleInstance)

[16. Java中引用类型的区别，具体的使用场景](#java_16_references)

[17. Exception 和 Error的区别](#java_17_exception&error)

[18. ](#)

[19. ](#)

[20. ](#)

[21. ](#)

[22. ](#)

[23. ](#)


## <span id="java_gc_1">1. GC 机制</span> 
> 垃圾回收要完成两件事：找到垃圾、回收垃圾。

### 1.1  找到垃圾有两种方法：

#### 1.1.1 引用计数法
&ensp;&ensp;&ensp;当一个对象被引用时，它的引用计数器会加一，垃圾回收时会清理掉引用计数为0的对象。但这种方法有一个问题，比方说有两个对象 A 和 B，A 引用了 B，B 又引用了 A，除此之外没有别的对象引用 A 和 B，那么 A 和 B 在我们看来已经是垃圾对象，需要被回收，但它们的引用计数不为 0，没有达到回收的条件。正因为这个循环引用的问题，**Java 并没有采用引用计数法。**

#### 1.1.2 可达性分析法
我们把 Java 中对象引用的关系看做一张图，从根级对象不可达的对象会被垃圾收集器清除。根级对象一般包括 **① Java 虚拟机栈中的对象** 、 **② 本地方法栈中的对象** 、 **③ 方法区中的静态对象** 和 **④ 常量池中的常量** 。
### 1.2 回收垃圾的方法有四种：
#### 1.2.1 标记清除算法
顾名思义分为两步，标记和清除。首先标记到需要回收的垃圾对象，然后回收掉这些垃圾对象。标记清除算法的缺点是清除垃圾对象后会造成 **内存的碎片化。**
#### 1.2.2 复制算法
复制算法是将存活的对象复制到另一块内存区域中，并做相应的内存整理工作。复制算法的优点是 **可以避免内存碎片化**，缺点也显而易见，它 **需要两倍的内存。**
#### 1.2.3 标记整理算法
标记整理算法也是分两步，**先标记后整理**。它会标记需要回收的垃圾对象，清除掉垃圾对象后会将存活的对象压缩，避免了内存的碎片化。
#### 1.2.4 分代算法
分代算法将对象分为 **新生代** 和 **老年代** 对象。那么为什么做这样的区分呢？主要是在Java运行中会产生大量对象，这些对象的生命周期会有很大的不同，有的生命周期很长，有的甚至使用一次之后就不再使用。所以针对不同生命周期的对象采用不同的回收策略，这样可以提高GC的效率。

新生代对象分为 **三个区域：Eden 区和两个 Survivor 区(① & ②)。** 新创建的对象都放在 Eden区，当 Eden 区的内存达到阈值之后会触发 **Minor GC**，这时会将存活的对象复制到一个 **Survivor(①)** 区中，这些存活对象的 **生命存活计数会加一**。这时 Eden 区会闲置，当再一次达到阈值触发 Minor GC 时，会将 **Eden区和之前一个 Survivor(①) 区** 中存活的对象复制到另一个 **Survivor(②)** 区中，采用的是我之前提到的 **复制算法**，同时它们的生命存活计数也会加一。
这个过程会持续很多遍，直到对象的 **存活计数达** 到一定的阈值后会触发一个叫做 **晋升** 的现象：**新生代的这个对象会被放置到老年代中。**
老年代中的对象都是经过多次 GC 依然存活的生命周期很长的 Java 对象。当老年代的内存达到阈值后会触发 **Major GC**，采用的是 **标记整理算法**。

[返回目录](#java_category)
## <span id="java_jvm_2">2. JVM 内存区域的划分，哪些区域会发生 OOM</span>
> JVM 的内存区域可以分为两类：线程私有区域；线程共有区域； 
### 2.1 线程私有区域
程序计数器、JVM 虚拟机栈、本地方法栈；

2.1.1 程序计数器：

每个线程有有一个私有的程序计数器，任何时间一个线程都只会有一个方法正在执行，也就是所谓的当前方法。程序计数器存放的就是这个当前方法的JVM指令地址。

2.1.2 JVM虚拟机栈:

创建线程的时候会创建线程内的虚拟机栈，栈中存放着一个个的栈帧，对应着一个个方法的调用。JVM 虚拟机栈有两种操作，分别是压栈和出站。栈帧中存放着局部变量表、方法返回值和方法的正常或异常退出的定义等等。

2.1.3 本地方法栈：

跟 JVM 虚拟机栈比较类似，只不过它支持的是 Native 方法。
### 2.2 线程共有区域：
堆、方法区、运行时常量池；

2.2.1 堆：

堆是内存管理的核心区域，用来存放对象实例。几乎所有创建的对象实例都会直接分配到堆上。所以堆也是垃圾回收的主要区域，垃圾收集器会对堆有着更细的划分，最常见的就是把堆划分为新生代和老年代。

2.2.2 方法区：

方法区主要存放类的结构信息，比如静态属性和方法等等。

2.2.3 运行时常量池：

运行时常量池位于方法区中，主要存放各种常量信息。


**其实除了程序计数器，其他部分都会发生 OOM**

* **堆**： 通常发生的 OOM 都会发生在堆中，最常见的可能导致 OOM 的原因就是内存泄漏。
* **JVM 虚拟机栈和本地方法栈**：当我们写一个递归方法，这个递归方法没有循环终止条件，最终会导致 StackOverflow 的错误。当然，如果栈空间扩展失败，也是会发生 OOM 的。
* **方法区**：现在基本上不会发生 OOM 但在早期内存中加载的类信息过多的情况下也会发生 OOM 的。 

[返回目录](#java_category)
## <span id="java_3_classloader">3. 类加载过程</span>
> Java 中类的加载分为 3 个步骤：加载、链接、初始化。

* **加载**：加载是将字节码数据从不同的数据源读取到 JVM 内存，并映射为 JVM 认可的数据结构(Class 对象)的过程。数据源可以是 Jar 文件、Class 文件等。如果数据源的格式不是 ClassFile 的结构，则会报 ClassFormatError。
* **链接**：链接是类加载的核心部分，一共分为三个步骤：验证、准备、解析
	* **验证**：验证是保证 JVM 安全的重要步骤。JVM 需要校验字节信息是否符合规范，避免恶意信息和不规范数据危害 JVM 运行安全。如果验证失败，则会报 VerifyError。
	* **准备**：这一步会创建静态变量，并为静态变量开辟内存空间。
	* **解析**：这一步会将符号引用替换为直接引用。

* **初始化**：初始化会为静态变量赋值，并执行静态代码中的逻辑。

[返回目录](#java_category)
## <span id="java_4_doubleParent">4. 双亲委派模型</span>
类的加载器大致分为 3 类：启动类加载器、扩展类加载器、应用程序类加载器。

* 启动类加载器：主要加载 `jre/lib` 下的 `jar` 文件。
* 扩展类加载器：主要加载 `jre/lib/ext` 下的 `jar` 文件。
* 应用程序类加载器：主要加载 `classpath` 下的文件。

所谓的 **双亲委派模型** 就是当加载一个类时，会优先使用父类加载器加载，当父类加载器无法加载时，才会使用子类加载器去加载。 这么做的目的是为了避免类的重复加载。

[返回目录](#java_category)
## <span id="java_5_set">5. Java 中的集合类 - HashMap</span>
### HashMap 的原理
HashMap 的内部可以看做 **数组 + 链表** 的复合结构。数组被分为一个个的桶(bucket)。哈希值决定了键值对在数组中的寻址。具有相同哈希值的键值对会组成链表。需要注意的是，当链表长度超过阈值(默认8)的时候会触发树化，链表会变成树形结构。

**把握 HashMap 的原理需要关注 4 个方法：hash、put、get、resize**

* **hash()**:将 key 的hashCode 值的高位数据移位到低位进行 **异或** 运算。这么做的原因是有些 key 的hashCode 值的差异集中在高位，而哈希寻址是忽略容量以上高位的，这种做法可以有效避免哈希冲突。
* **put()**:  put 方法主要有一下几个步骤：
	* 通过 hash 方法获取 hash 值，根据 hash 值寻址。
	* 如果未发生碰撞，直接放到桶中。
	* 如果发生碰撞，则以链表形式放在桶后。
	* 当链表长度大于阈值后会触发树化，将链表转为红黑树。
	* 如果数组长度达到阈值，会调用 resize 方法扩展容量。
* **get()**：get 方法主要有一下几个步骤：
	* 通过 hash 方法获取 hash 值，根据 hash 值寻址。
	* 如果寻址到桶的 key 值相等，直接返回对应的 value。
	* 如果发生冲突，分两种情况：如果是树，则调用 getTreeNode 获取 value；如果是链表，则通过循环遍历查找对应的 value。
* **resize()**：resize 做了两件事：
	* 将原数组扩展为原来的 2 倍。
	* 重新计算 index 索引值，将原节点重新放到新的数组中。这一步可以将原先冲突的节点分散到新的桶中。
	* 
[返回目录](#java_category)

##  <span id="java_6_synchronized">6. Java 死锁的产生，如何定位、修复，死锁 Demo</span>
##  <span id="java_7_sleep&wait">7. sleep & wait 的区别</span>
* sleep 方法是 Thread 类中的静态方法，wait 是 Object 类中的方法。
* sleep 并不会释放同步锁，而 wait 会释放同步锁。
* sleep 可以在任何地方使用，而 wait 只能在同步方法或者同步代码块中使用。
* sleep 必须转入时间，而 wait 可以传或不传，不传时间的话，则只有 notify 或 notifyAll 才能唤醒，传时间的话在时间之后会自动唤醒。

##  <span id="java_7_join">8. join 的用法</span>
join 方法通常是保证线程间顺序调度的一个方法，它是 Thread 类中的方法。比方说在线程 A 中执行线程 `B.join()`，这时线程 A 会进入等待状态，直到线程 B 执行完毕之后才会唤醒，继续执行 A 线程中的后续方法。

`join()` 可以传时间参数，或不传。不传参数实际上调用的时 `join(0)`。它的原理其实是使用了 `wait` 方法。join 的原理如下：

	public final synchronized void join(long millis) throws InterruptedException{
		long base = System.currentTimeMillis();
		long now = 0;
		if(millis < 0){
			throw new IllegalArgumentException("timeout value is negative");
		}

		if(millis == 0){
			while(isAlive()){
				wait(0);
			}
		}else{
			while(isAlive()){
				long delay = millis - now;
				if(delay <= 0){
					break;
				}
				wait(delay);
				now = System.currentTimeMillis() - base;
			}
		}
	}

## <span id="java_9_volatile&synchronized">9. volatile和synchronize的区别</span>
## <span id="java_10_threadPool">10. Java中的线程池</span>

## <span id="java_11_threadCommunication">11. 线程通信</span>

## <span id="java_12_concurrentSet">12. Java中的并发集合</span>

## <span id="java_13_producer&consumer">13. Java中生产者与消费者模式</span>
生产者消费者模式 要保证的是当前缓冲区满的时候，生产者不再生产对象，当缓冲区空的时候，消费者不再消费对象。实现机制就是当缓冲区满时，让生产者处于等待状态，当缓冲区为空时，让消费者处于等待状态。当生产者生产了一个对象后，会唤醒消费者，当消费者消费一个对象后会唤醒生产者。

**三种实现方式：1. wait & notify；2. wait & signal；3. BlockingQueue**

* **wait & notify**


	import java.util.LinkedList;

	public class StorageWithWaitAndNotify {

		private final int MAX_SIZE = 10;
		private LinkedList<Object> list = new LinkedList<Object>();
		
		public void produce(){
			synchronized(list){
				while(list.size == MAX_SIZE){
					System.out.println("仓库已满：生产暂停")；
					try{
						list.wait();
					}catch(InterruptedException e){
						e.printStackTrace();
					}
				}
				list.add(new Object());
				System.out.println("生产了一个新产品，现库存为："+list.size())；
				list.notifyAll();
			}
		}
		public void consume(){
			synchronized(list){
				while(list.size == 0){
					System.out.println("仓库已空：消费暂停")；
					try{
						list.wait();
					}catch(InterruptedException e){
						e.printStackTrace();
					}
				}
				list.remove();
				System.out.println("消费了一个新产品，现库存为："+list.size())；
				// 上面使用相同的锁 list,所以可以相互唤醒。
				list.notifyAll();
			}
		}	
	}
	
* **await & signal**

	import java.util.LinkedList;
	import java.util.concurrent.locks.Condition;
	import java.util.concurrent.locks.ReentrantLock;
	
	class StorageWithAwaitAndSignal {
	    private final int                MAX_SIZE = 10;
	    private       ReentrantLock      mLock    = new ReentrantLock();
	    private       Condition          mEmpty   = mLock.newCondition();
	    private       Condition          mFull    = mLock.newCondition();
	    private       LinkedList<Object> mList    = new LinkedList<Object>();
	
	    public void produce() {
	        mLock.lock();
	        while (mList.size() == MAX_SIZE) {
	            System.out.println("缓冲区满，暂停生产");
	            try {
	                mFull.await();
	            } catch (InterruptedException e) {
	                e.printStackTrace();
	            }
	        }
	
	        mList.add(new Object());
	        System.out.println("生产了一个新产品，现容量为：" + mList.size());
	        mEmpty.signalAll();
	
	        mLock.unlock();
	    }
	
	    public void consume() {
	        mLock.lock();
	        while (mList.size() == 0) {
	            System.out.println("缓冲区为空，暂停消费");
	            try {
	                mEmpty.await();
	            } catch (InterruptedException e) {
	                e.printStackTrace();
	            }
	        }
	
	        mList.remove();
	        System.out.println("消费了一个产品，现容量为：" + mList.size());
	        mFull.signalAll();
	
	        mLock.unlock();
	    }
	}
* **BlockingQueue**

	import java.util.concurrent.LinkedBlockingQueue;
	
	public class StorageWithBlockingQueue {
	    private final int MAX_SIZE = 10;
	    private LinkedBlockingQueue<Object> list = new LinkedBlockingQueue<Object>(MAX_SIZE);
	    public void produce() {
	        if (list.size() == MAX_SIZE) {
	            System.out.println("缓冲区已满，暂停生产");
	        }
	
	        try {
	            list.put(new Object());
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }
	        System.out.println("生产了一个产品，现容量为：" + list.size());
	    }
	
	    public void consume() {
	        if (list.size() == 0) {
	            System.out.println("缓冲区为空，暂停消费");
	        }
	
	        try {
	            list.take();
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }
	
	        System.out.println("消费了一个产品，现容量为：" + list.size());
	    }
	}

## <span id="java_14_finalXXX">14. final、finally、finalize区别</span>

- final 可以修饰类、变量和方法。修饰类代表这个类不可被继承。修饰变量代表此变量不可被改变。修饰方法表示此方法不可被重写 (override)。
- finally 是保证重点代码一定会执行的一种机制。通常是使用 try-finally 或者 try-catch-finally 来进行文件流的关闭等操作。
- finalize 是 Object 类中的一个方法，它的设计目的是保证对象在垃圾收集前完成特定资源的回收。finalize 机制现在已经不推荐使用，并且在 JDK 9已经被标记为 deprecated。

## <span id="java_15_singleInstance">15. Java 单例模式</span>
Java 中常见的单例模式实现有这么几种：饿汉式、双重判断的懒汉式、静态内部类实现的单例、枚举实现的单例。 这里着重讲一下双重判断的懒汉式和静态内部类实现的单例。

## <span id="java_16_references">16. Java中引用类型的区别，具体的使用场景</span>

## <span id="java_17_exception&error">17. Exception 和 Error的区别</span>

## <span id=""></span>

## <span id=""></span>

## <span id=""></span>

## <span id=""></span>

## <span id=""></span>

## <span id=""></span>
[返回目录](#java_category)


[返回目录](#java_category)


[返回目录](#java_category)


[返回目录](#java_category)


[返回目录](#java_category)


[返回目录](#java_category)


[返回目录](#java_category)


[返回目录](#java_category)


[返回目录](#java_category)

[返回目录](#java_category)


[返回目录](#java_category)


[返回目录](#java_category)


[返回目录](#java_category)

[返回目录](#java_category)

[返回目录](#java_category)

[返回目录](#java_category)