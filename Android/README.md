# Android Interview
## 1. 对 Context 的理解：
Context 是一个包含上下文信息(外部值)的一个参数。
Android 中的 Context 分为三种：

* Application Context;
* Activity Context;
* Service Context;

它 描述的是一个应用程序环境的信息，通过它我们可以获取应用程序的资源和类，也包括一些应用级别的操作。
例如：启动 Activity、发送广播、接收 Intent 信息等。